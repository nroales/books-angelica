package books.repository;

import books.model.Book;

import java.util.ArrayList;
import java.util.List;

public class BooksRepository {

    private final List<Book> books;

    public BooksRepository(){
        this.books = new ArrayList<>();
    }

    public List<Book> get(){
        return books;
    }

    public Book get(String title){
        for(Book book: books){
            if(title.equals(book.getTitle())){
                return book;
            }
        }
        return null;
    }

    public void add(Book book){
        books.add(book);
    }

}
