package books.service;

import books.exception.BookAlreadyExistsException;
import books.exception.BookNotFoundException;
import books.model.Book;
import books.repository.BooksRepository;

import java.util.List;

public class BooksService {

    private final BooksRepository repository;

    public BooksService(){
        this.repository = new BooksRepository();
    }

    public List<Book> get(){
        return repository.get();
    }

    public Book get(String title) throws BookNotFoundException {
        assert title != null;

        Book foundBook = repository.get(title);
        if(foundBook == null){
            throw new BookNotFoundException();
        }

        return foundBook;
    }

    public void add(Book book) throws BookAlreadyExistsException {
        assert book != null && book.getTitle() != null;

        if(repository.get(book.getTitle()) != null){
            throw new BookAlreadyExistsException();
        }
        repository.add(book);
    }
}
