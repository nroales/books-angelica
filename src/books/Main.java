package books;

import books.model.Book;
import books.service.BooksService;

public class Main {

    public static void main(String[] args) throws Exception {

        BooksService service = new BooksService();

        Book firstBook = new Book("El Libro de la Selva");
        Book secondBook = new Book("La Bella y la Bestia");

        service.add(firstBook);
        service.add(secondBook);

        for(Book book: service.get()){
            System.out.println("Título: " + book.getTitle());
        }
    }
}
